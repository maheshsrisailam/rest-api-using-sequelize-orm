const dotenv = require('dotenv')
const express = require('express')
const router = express.Router()

const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const logger = require('../logger.js')

const sequelize = require('../dbConnection')
const authenticationMiddleware = require('../authenticationMiddleware')
const {isValidName,isValidEmail,isValidPassword,isValidTitleContent} = require('../validations')
const {user, task, subtask,refreshtoken} = require('../models')
dotenv.config()


//Sign Up or Register API
router.post('/signup', async (request,response) => {
    try {
        const {name,email,password} = request.body
        if (!isValidName(request.body.name)) {
            response.status(400).send(`Invalid name. Name contains only alphabets.`)
        } else if (!isValidEmail(request.body.email)){
            response.status(400).send(`${request.body.email} : Invalid Email`)
        } else if (!isValidPassword(request.body.password)) {
            response.status(400).send(`Characters in the password must be lies between 6 to 255`)
        } else {
            const hashedPassword = await bcrypt.hash(password,10)
            const isUserFound = await user.findOne({
                where: {email :  request.body.email}
            });
            if (isUserFound) {
                response.status(400).send(`User with Email: ${email} is already exists in the database.`)
            } else {
                const userData = await user.create({
                name,
                email,
                password: hashedPassword
            })
            response.status(200).send(userData)
            }
        }
        
    }catch (error) {
        response.status(500).send(error.message)
    }
});


//Login API

router.post('/login', async (request,response) => {
    try {    
        const isUserFound = await user.findOne({
            where: {email :  request.body.email}
        });
        if (isUserFound) {
            const isPasswordMatched = await bcrypt.compare(request.body.password,isUserFound.password)
            if (isPasswordMatched) {
                const payload = {
                    email: request.body.email
                }
                const jwtToken = generateAccessToken(payload)
                const refreshToken = jwt.sign(payload, process.env.REFRESH_KEY)
                await refreshtoken.create({
                    email: request.body.email,
                    token: refreshToken
                })
                response.status(200).json({jwtToken,refreshToken})
            } else{
                response.status(401).send(`Password is incorrect.`)
            }
        } else {
            response.status(404).send(`User with EMAIL: ${request.body.email} is not found.`)
        }
    } catch (error) {
        response.status(500).send(error.message)
    }
})

function generateAccessToken(payload) {
    return jwt.sign(payload, process.env.SECRET_KEY,{expiresIn:"1m"})
}

//API to generate new access token

router.post('/token', async (request,response)=>{
    try {
        if (request.body.token === null) {
            logger.error("Unauthorized User")
            response.status(401).send("Unauthorized user")
        } else {
            const isTokenFound = await refreshtoken.findOne({
                where: {token: request.body.token}
            })
            if (isTokenFound) {
                jwt.verify(request.body.token,process.env.REFRESH_KEY, (error,payload)=>{
                    if (error) {
                        return response.status(403).send("Invalid Refresh Token")
                    }
                    const accessToken = generateAccessToken({email:payload.email})
                    logger.info("New Access Token Generated.")
                    response.send({accessToken})
                })
            } else {
                response.status(404).send(`Refresh token is not found`)
            }
        }
    } catch (error) {
        response.status(500).send(error.message)
    }
});

//API to read the user's data from the users table.

router.get('/users',authenticationMiddleware, async (request,response) => {
    try {
        const usersData = await user.findAll()
        response.status(200).send(usersData)
    } catch (error) {
        response.status(500).send(error.message)
    }
});

//API to read the specific task's data from the tasks table.

router.get('/users/task/:id',authenticationMiddleware, async (request,response) => {
    try {
        const taskData = await task.findOne({
            where:{
                task_id: request.params.id
            }
        })
        if (taskData) {
            response.status(200).send(taskData)
        } else {
            response.status(404).send(`Task with task_id: ${request.params.id} is not found.`)
        }
    } catch (error) {
        response.status(500).send(error.message)
    }
});

//API to read the specific subtask's data from the subtasks table table.

router.get('/users/subtask/:id',authenticationMiddleware, async (request,response) => {
    try {
        const subtaskData = await subtask.findOne({
            where:{
                subtask_id: request.params.id
            }
        })
        if (subtaskData) {
            response.status(200).send(subtaskData)
        } else {
            response.status(404).send(`Subtask with subtask_id: ${request.params.id} is not found.`)
        }
    } catch (error) {
        response.status(500).send(error.message)
    }
});

//API to read the tasks's data from the tasks table.

router.get('/tasks',authenticationMiddleware, async (request,response) => {
    try {
        const tasksData = await task.findAll()
        response.status(200).send(tasksData)
    } catch (error) {
        response.status(500).send(error.message)
    }
});

//API to read the subtask's data from the subtasks table.

router.get('/subtasks',authenticationMiddleware, async (request,response) => {
    try {
        const subtasksData = await subtask.findAll()
        response.status(200).send(subtasksData)
    } catch (error) {
        response.status(500).send(error.message)
    }
});

//API to update the user's data in the users table.

router.put('/users/:id', authenticationMiddleware, async (request,response) => {
    try {
        const isValidUserToUpdate = await user.findOne({
            where: {
                id: request.params.id
            }
        });
        const isValidUser = isValidUserToUpdate.email == request.email
        if(isValidUserToUpdate.length === 0) {
            response.status(404).send(`User with ID: ${request.params.id} is not found in the database.`)
        } else if (!isValidUser) {
            response.status(404).send(`User with EMAIL: ${request.email} has no access to update the user's data of USER_ID: ${request.params.id}.`)
        } else {
            
            await user.update({name: request.body.name,email:request.body.email}, {
                where: {
                    id: request.params.id
                }
            })
            response.status(200).send(`User with ID: ${request.params.id} is updated sucessfully`)
        }
    } catch (error) {
        response.status(500).send(error.message)
    }
})

//API to update the task's data in the task table.

router.put('/users/task/:task_id', authenticationMiddleware, async (request,response) => {
    try {
        const userData = await user.findOne({where : {email : request.email}})
        const taskData = await task.findOne({where: {userId: userData.id,task_id: request.params.task_id}})
        
        if (taskData) {
            await task.update({title: request.body.title,userId:request.body.userId,is_completed: request.body.is_completed}, {
                where: {
                    task_id: request.params.task_id
                }
            })
            response.status(200).send(`Task with ID: ${request.params.task_id} is updated sucessfully`)
        } else {
            response.status(401).send(`User with EMAIL: ${request.email} is not allowed to update the task with ID: ${request.params.task_id} or ID: ${request.params.task_id} does not exists`)
        }
    } catch (error) {
        response.status(500).send(error.message)
    }
})
// //API to delete the user data from the users table.

router.delete('/users/delete/:id', authenticationMiddleware, async (request,response) => {
    try {
        const loggedInUser = await user.findOne({where : {email : request.email}})
        const isUserFound = await user.findOne({where : {id: request.params.id}})
        
        if (isUserFound === null) {
            response.status(404).send(`User with ID: ${request.params.id} is not found in the database.`)
        } else if (loggedInUser.id !== isUserFound.id){
            response.status(401).send(`No access to delete the User with ID: ${request.params.id}.`)
        } else {
            await user.destroy({
                where: {
                    id: request.params.id
                }
            })
            await refreshtoken.destroy({where:{email: request.email}})
            response.status(200).send(`User with ID: ${request.params.id} is deleted sucessfully`)
        }
    } catch (error) {
        response.status(500).send(error.message)
    }
});

//Adding new task to the task table

router.post('/users/tasks',authenticationMiddleware, async (request,response) => {
    try {
        const isTasksFound = await task.findAll({where: {title: request.body.title}})
        if (isTasksFound.length !== 0) {
            response.status(400).status(`Task already exists in the tasks table.`)
        } else if (!(isValidTitleContent(request.body.title))) {
            response.status(400).send(`Characters in the title lies between 20 to 255`)
        }else {
            const newTask = await task.create({
                title :  request.body.title,
                userId : request.body.userId,
                is_completed: request.body.is_completed
            });
            response.status(200).send(newTask)
        }
    }catch (error) {
        response.status(500).send(error.message)
    }
});

//Adding new sub task

router.post('/users/subtasks',authenticationMiddleware, async (request,response) => {
    try {
        const isSubtasksFound = await subtask.findAll({where: {title: request.body.title}})
        if (isSubtasksFound.length !== 0) {
            response.status(400).status(`Subtask already exists in the subtasks table.`)
        } else if (!(isValidTitleContent(request.body.title))) {
            response.status(400).send(`Characters in the title lies between 20 to 255`)
        }else {
            const newSubtask = await subtask.create({
                title :  request.body.title,
                taskTaskId : request.body.taskId,
                is_completed: request.body.is_completed
            });
            response.status(200).send(newSubtask)
        }
    }catch (error) {
        response.status(500).send(error.message)
    }
});

// //API to logout from the application

router.delete('/logout', authenticationMiddleware, async (request,response) => {
    try {
        const {email} = request
        const isUserFound = await refreshtoken.findOne({
            where:{email: email}
        })
        if (isUserFound) {
            await refreshtoken.destroy({
                where : {
                    email:email
                }
            })
            response.status(200).send(`The user with EMAIL: ${email} is logged out successfylly.`)

        }
    } catch (error) {
        response.status(500).send(error.message)
    }
})

module.exports = router;