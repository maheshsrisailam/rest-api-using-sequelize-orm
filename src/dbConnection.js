const {Sequelize} = require('sequelize');
const logger = require('./logger')

const sequelize = new Sequelize("entitiesdatabase","mahesh","Mahesh@7777",{
    host:"db4free.net",
    dialect: "mysql"
});

const initialisingSequelizeConnection = async () => {
    try {
        await sequelize.authenticate()
        logger.info("Database connection enabled successfully.")
    } catch (error) {
        logger.error(`ERROR: ${error.message} while connecting.`)
    }
}

initialisingSequelizeConnection()

module.exports = sequelize;