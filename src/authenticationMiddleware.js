const jwt = require('jsonwebtoken')
const logger = require("./logger")

const authenticationMiddleware = (request,respone,next) => {
    try {
        let jwtToken = null
        const authorizationHeader = request.headers["authorization"]
        if (authorizationHeader !== undefined) {
            jwtToken = authorizationHeader.split(" ")[1]
        }

        if (jwtToken !== undefined) {
            jwt.verify(jwtToken,process.env.SECRET_KEY, (error,payload) => {
                if (error) {
                    respone.status(403).send("Invalid Access Token.")
                    logger.error("Invalid Access Token.")
                } else {
                    request.email = payload.email
                    next()
                }
            })
        } else {
            respone.status(404).send("JWT Token in not found")
        }
    } catch (error) {
        respone.status(500).send(error.message)
    }

}

module.exports = authenticationMiddleware;
