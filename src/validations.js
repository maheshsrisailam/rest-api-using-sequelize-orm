const isValidName = (name) => {
    const regex = /^[a-zA-Z ]{3,30}$/;
    return regex.test(name)
}

const isValidEmail = (email) => {
    const regex = /^[a-z0-9]+@[a-z]+\.[a-z]{2,3}$/;
    return regex.test(email)
}

const isValidPassword = (password) => {
    return  ((password.length >= 5) && (password.length <= 255)) 
}

const isValidTitleContent = (title) => {
    return ((title.length >= 20) && (title.length <= 255))
}

module.exports = {isValidName,isValidEmail,isValidPassword,isValidTitleContent}
