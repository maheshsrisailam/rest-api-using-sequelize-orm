const express = require('express');
const logger = require("./src/logger")

const app = express()

app.use(express.json())

app.use('/api', require('./src/routes/apis'))

app.use((request,response,next) => {
    const error = new Error("NOT FOUND")
    next(error)
})

app.use((error,request,response,next) => {
     response.status(404 || 500).send(error.message)
});


const PORT = process.env.PORT || 3000

const initailisingServer = () => {
    try {
        app.listen(PORT, () => logger.info(`Server Started listening on the http://localhost:${PORT}`))
    } catch (error) {
        logger.error(`ERROR: ${error.message} while initialising the server.`)
        process.exit(1)
    }
};

initailisingServer();